# Kryptonite lock adapter for a bicycle rack

![](pictures/DSC_6379.jpg)

**What is it?**
Adapter for Kryptonite bike lock to be mounted on a bicycle rack. For all lock types that has this [FLEXFRAME-U Bracket. Like - New-U Evolution](https://www.amazon.ca/Kryptonite-New-U-Evolution-Mini-5-Bicycle/dp/B06XCTDYS6/ref=sr_1_4?dchild=1&keywords=kryptonite+lock&qid=1630345218&sr=8-4).

**Why build it?**

Using provided attachment bracket, lock did not fit anywhere around the frame. It probably works on most of the bikes but not on this specific one. Therefore putting it on a rack it seems like a quick access option and I don't use rack for carrying things. I rather used side bags on the rack therefore top of the rack was available so I made this adapter.

It can be printed without support. But I would recommend using brim.

**BOM**

![](pictures/DSC_6361.jpg)

1x Adapter
4x zip ties
1x M4x20 bolt
1x M4 nut
Some rubber or electric tape to protect the rack

**Assembly**

* Sand, prime, paint
![](pictures/DSC_6365.jpg)
* Insert Nut
![](pictures/DSC_6366.jpg)
* Attach adapter using M4 bolt
![](pictures/DSC_6368.jpg)
![](pictures/DSC_6371.jpg)
* Insert zip ties
![](pictures/DSC_6378.jpg)
* Install adapter to the rack using some rubber in between
![](pictures/DSC_6443.jpg)
![](pictures/DSC_6445.jpg)
![](pictures/DSC_6448.jpg)
![](pictures/DSC_6457.jpg)
